#include "ros/ros.h"
#include "benchmark_latency/LatencyMessage.h"
#include <std_msgs/Int64.h>
#include <inttypes.h>

/** 
 * Latency callback function. Called when message received from the publisher.
 * Specify the message type, and a publisher handle, which will send the latency.
 */
void latencyCallback(const benchmark_latency::LatencyMessage::ConstPtr& msg, ros::Publisher& pub) {
  /**
   * Record the time as soon as possible (right after its call), and log it.
   */
  ros::Time time_received = ros::Time::now();
  int64_t latency = time_received.toNSec() - msg->time_sent.toNSec();
  ROS_INFO("Latency is : %" PRIu64, latency);
  /**
   * Create, fill and send the message that contains the latency.
   */
  std_msgs::Int64 lat_msg;
  lat_msg.data = latency;
  pub.publish(lat_msg);
}

int main(int argc, char **argv) {
  /**
   * Initialize ROS environment and a node name, latency_subscriber.
   */
  ros::init(argc, argv, "latency_subscriber");

  /**
   * Create the handle for the node and actually initialize it.
   */
  ros::NodeHandle n;
  
  /**
   * Node will publish on the topic benchmark_latency_reporting to the reporter node.
   */
  ros::Publisher pub = n.advertise<std_msgs::Int64>("benchmark_latency_reporting", 10);

  /**
   * Node will receive the actual message on the topic on benchmark_latency.
   * If it receives a message on this topic, latencyCallback is called.
   * The nodes own publisher is added by binding it to the callback.
   * boost::bind will pass the pub variable to the function, _1 is the number of placeholders,
   * because the first parameter of the function is the message pointer.
   * Basically the same as calling latencyCallback(latencyMessage, pub), but there is no other way
   * in ROS to pass a function with more parameters to subscribe.
   */ 
  ros::Subscriber latency_sub = n.subscribe<benchmark_latency::LatencyMessage>("benchmark_latency", 10, 
    boost::bind(latencyCallback, _1, pub));
    
  /**
   * The node will be alive and receive messages until shutdown message received.
   */
  ros::spin();

  return 0;
}