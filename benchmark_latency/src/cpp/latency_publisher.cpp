#include <ros/ros.h>
#include "benchmark_latency/LatencyMessage.h"

#include <inttypes.h>

int main(int argc, char **argv) {
  /**
   * Check if all the parameters have been passed to initialise the node.
   * If not, print the usage and exit.
   */
  if (argc != 3)
    std::cout << "USAGE: latency_publisher [MESSAGE_SIZE] [FREQUENCY]" << std::endl;
  else {
    /**
     * Initialize the variables.
     * Use the command line argument from **argv
     */
    int message_size = atoi(argv[1]);
    int frequency = atoi(argv[2]);

    /**
     * Initialize ROS environment and a node name, latency_publisher.
     */
    ros::init(argc, argv, "latency_publisher");

    /**
     * Create the handle for the node, and actually initialize it.
     */
    ros::NodeHandle n;

    /**
     * Node will publish on the topic of benchmark_latency to the subscriber node.
     */
    ros::Publisher pub = n.advertise<benchmark_latency::LatencyMessage>("benchmark_latency", 10);
    
    /**
     * To send the messages on a given frequency, the Rate should be initialized.
     */
    ros::Rate loop_rate(frequency);

    /**
     * While the node is up (not shut down) it keeps publishing.
     */
    while (ros::ok()) {
      /**
       * Create the latency message.
       */
      benchmark_latency::LatencyMessage msg;

      /**
       * Create a message_size length vector filled with 0.
       * Each element is 1 byte in the memory.
       * Assign it to the message.
       */
      std::vector<uint8_t> _data(message_size, 0);
      msg.data = _data;

      /**
       * Record the time of sending, right before the sending, to have the most accurate measurement.
       */
      msg.time_sent = ros::Time::now();

      /**
       * Send the message.
       */
      pub.publish(msg);

      /**
       * Log that the message was sent.
       */
      ROS_INFO("Message sent at %" PRIu64, msg.time_sent.toNSec());

      ros::spinOnce();
      
      /**
       * Send the node to sleep until it needs to publish again, based on the frequency.
       */
      loop_rate.sleep();
    }
  }

  return 0;
}