#!/usr/bin/env python

import rospy
import sys

from benchmark_latency.msg import LatencyMessage

def latency_publisher(message_size, frequency):
    """
    Setup the latency_publisher node to publish messages to the benchmark_latency topic at the given message size and frequency.
    """
    pub = rospy.Publisher('benchmark_latency', LatencyMessage, queue_size=10)
    rospy.init_node('latency_publisher', anonymous=True)
    rate = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        # Create a message_size length array filled with 0 as message data.
        # Each element is 1 byte in the memory.        
        message = LatencyMessage(data=([0] * message_size))

        # Record the time right before sending to have the most accurate measurement.
        message.time_sent = rospy.get_rostime()
        try :
            pub.publish(message)
        except rospy.ROSException:
            rospy.signal_shutdown('benchmark_latency topic closed')

        rospy.loginfo('Message sent at %s', message.time_sent.to_nsec())
        rate.sleep()

if __name__ == '__main__':
    # Parse args and pass to publisher.
    args = sys.argv
    if len(args) != 3:
        print 'USAGE: latency_publisher.py [MESSAGE_SIZE] [FREQUENCY]'
    else:
        message_size = int(args[1])
        frequency = int(args[2])
        try:
            latency_publisher(message_size, frequency)
        except rospy.ROSInterruptException:
            pass