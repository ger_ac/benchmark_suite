#!/usr/bin/env python

import sys
import rospy
import csv
from os import path

from std_msgs.msg import Int64

latency_report = []

def latency_reporter(message_size, frequency, number_of_messages, implementation):
    """
    Setup the latency_reporter node to listen to the benchmark_latency_reporting topic and record number of messages,
    and shut down when the required number of messages has been recorded.
    """
    def reporting_callback(message):
        """
        Record a message reporting a given latency, produce report when enough messages recorded and shut down.
        """
        global number_of_messages
        global latency_report 

        if number_of_messages >= 1:
            rospy.loginfo('Reporting latency: %s', message.data)
            latency_report.append(message.data)            
            number_of_messages -= 1
        else:
            rospy.loginfo('All messages received, shutting down reporter')

            export_to_csv('benchmark_latency.csv', message_size, frequency, implementation)
            rospy.signal_shutdown('All messages received, shutting down')
    
    rospy.init_node('latency_reporter', anonymous=True)
    rospy.Subscriber('benchmark_latency_reporting', Int64, reporting_callback, queue_size=10)

    # Keep the node running until it shuts down.
    rospy.spin()

def export_to_csv(file_name, message_size, frequency, implementation):
    """
    Export the benchmark data to csv format, appending if the file already exists.
    """

    global latency_report
    has_existing_report = path.exists(file_name)

    file_mode = 'a+' if has_existing_report else 'w+'

    with open(file_name, file_mode) as f:
        writer = csv.writer(f)

        if not has_existing_report:
            # Write header row. 
            writer.writerow(['latency', 'message_size', 'frequency', 'implementation'])

        for latency in latency_report:
            writer.writerow([latency, message_size, frequency, implementation])

if __name__ == '__main__':
    # Parse args and pass to publisher.
    args = sys.argv
    if len(args)  != 5:
        print 'USAGE: latency_reporter.py [MESSAGE_SIZE] [FREQUENCY] [NUMBER_OF_MESSAGES] [IMPLEMENTATION]'
    else:
        message_size = int(args[1])
        frequency = int(args[2])
        number_of_messages = int(args[3])
        implementation = str(args[4])
        try:
            latency_reporter(message_size, frequency, number_of_messages, implementation)
        except rospy.ROSInterruptException:
            pass