#!/usr/bin/env python

import rospy

from std_msgs.msg import Int64
from benchmark_latency.msg import LatencyMessage


def latency_subscriber():
    """
    Setup the latency_subscriber node to listen to the benchmark_latency topic.
    """

    # Setup subscriber and publishing to reporting.
    reporting_publisher = rospy.Publisher('benchmark_latency_reporting', Int64, queue_size=10)

    def latency_callback(message):
        """
        Measure the latency and publish to the reporter node.
        """
        time_received = rospy.get_rostime()
        latency = time_received.to_nsec() - message.time_sent.to_nsec()
        rospy.loginfo('Latency is : %s', latency)
        
        # Send to reporting node.
        try :
            reporting_publisher.publish(Int64(data=latency))
        except rospy.ROSException:
            rospy.signal_shutdown('benchmark_latency_reporting topic closed')

    rospy.init_node('latency_subscriber', anonymous=True)

    rospy.Subscriber('benchmark_latency', LatencyMessage, latency_callback, queue_size=10)

    rospy.spin()

if __name__ == '__main__':
    latency_subscriber()