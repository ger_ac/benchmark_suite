#!/usr/bin/env python

import rosgraph
import subprocess
import sys
import time
import os

def latency_boss(message_size, frequency, number_of_messages, implementation):
    """
    Run the publisher, subscriber and reporter nodes.
    """

    implementation_suffix = '.py' if implementation == 'python' else ''

    publisher_cmd = 'rosrun benchmark_latency latency_publisher%s %s %s' % (implementation_suffix, message_size, frequency)
    subscriber_cmd = 'rosrun benchmark_latency latency_subscriber%s' % (implementation_suffix)
    reporter_cmd = 'rosrun benchmark_latency latency_reporter.py %s %s %s %s' % (message_size, frequency, number_of_messages, implementation)

    start = time.time()
    publisher_process = subprocess.Popen(publisher_cmd, shell=True)
    subscriber_process = subprocess.Popen(subscriber_cmd, shell=True)
    reporter_process = subprocess.Popen(reporter_cmd, shell=True)
    
    reporter_process.wait()

    os.system('rosnode kill -a')

    end = time.time()

    print 'Latency benchmark completed in %.2fs' % (end - start)

def args_are_valid(args):
    """
    Return whether the given arguments are valid for using in the benchmark.
    """
    if len(args) != 5:
        print 'USAGE: latency_boss.py [MESSAGE_SIZE] [FREQUENCY] [NUMBER_OF_MESSAGES] [IMPLEMENTATION]'
        return False
    elif not rosgraph.is_master_online():
        print 'ROS Master not online, please run roscore'
        return False
    elif args[4] != 'python' and args[4] != 'c++':
        print 'Unrecognized implementation: ' + str(args[4]) + '\nPlease choose c++ or python'
        return False
    else:
        return True

if __name__ == '__main__':
    args = sys.argv

    if args_are_valid(args):
        message_size = args[1]
        frequency = args[2]
        number_of_messages = args[3]
        implementation = args[4]

        latency_boss(message_size, frequency, number_of_messages, implementation)