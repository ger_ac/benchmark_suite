#include <ros/ros.h>
#include "benchmark_throughput/ThroughputMessage.h"

int main(int argc, char **argv) {
  /**
   * Check if all the parameters have been passed to initialise the node.
   * If not, print the usage and exit.
   */
  if (argc != 3)
    std::cout << "USAGE: throughput_publisher [MESSAGE_SIZE] [FREQUENCY]" << std::endl;
  else {
    /**
     * Initialize the variables.
     * Use the command line arguments from **argv
     */
    int message_size = atoi(argv[1]);
    int frequency = atoi(argv[2]);

    /**
     * Initialize ROS environment and a node name, throughput_publisher.
     */
    ros::init(argc, argv, "throughput_publisher");

    /**
     * Create the handle for the node, and actually initialize it.
     */
    ros::NodeHandle n;
    
    /**
     * Node will publish on the topic of benchmark_throughout to the subscriber node.
     */
    ros::Publisher pub = n.advertise<benchmark_throughput::ThroughputMessage>("benchmark_throughput", 10);
    
    /**
     * To send the messages on a given frequency, the Rate should be initialized.
     */
    ros::Rate loop_rate(frequency);

    /**
     * While the node is up (not shut down) it keeps publishing.
     */
    while (ros::ok()) {
      /**
       * Create the throughput message.
       */
      benchmark_throughput::ThroughputMessage msg;

      /**
       * Create a message_size length vector filled with 0.
       * Each element is 1 byte in the memory.
       * Assign it to the message.
       */
      std::vector<uint8_t> _data(message_size, 0);
      msg.data = _data;

      /**
       * Send the message.
       */
      pub.publish(msg);

      /**
       * Log that the message was sent.
       */
      ROS_INFO("Message sent");

      ros::spinOnce();
      
      /**
       * Send the node to sleep until it needs to publish again, based on the frequency.
       */
      loop_rate.sleep();
    }
  }

  return 0;
}