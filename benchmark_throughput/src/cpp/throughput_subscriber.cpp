#include "ros/ros.h"
#include "benchmark_throughput/ThroughputMessage.h"
#include "benchmark_throughput/ThroughputReportingMessage.h"

/** 
 * Throughput callback function. Called when message received from the throughput_publisher.
 * Increase the message counter when a message arrives.
 */
void throughputCallback(const benchmark_throughput::ThroughputMessage::ConstPtr& msg, int64_t& stop, int64_t& counter, ros::Publisher& pub) {
  /**
   * Start test with the first received message.
   */
  if (counter == 0) {
    stop += ros::Time::now().toNSec();
  }
  if (ros::Time::now().toNSec() > stop) {
    /**
     * Send a message with counter = -1 to the reporter to indicate that the test has ended.
     * Shut down the node after the test has finished.
     */
    benchmark_throughput::ThroughputReportingMessage reporting_msg;

    reporting_msg.message_count = -1;
    reporting_msg.time_received = ros::Time::now();
    
    pub.publish(reporting_msg);
    ros::shutdown();
  }
  else {
    ++counter;
    /**
     * Create, fill and send the message that contains the number of received messages.
     */
    benchmark_throughput::ThroughputReportingMessage reporting_msg;
    
    reporting_msg.message_count = counter;
    reporting_msg.time_received = ros::Time::now();
    
    pub.publish(reporting_msg);
    ROS_INFO("Message received %ld", counter);
  }
}

int main(int argc, char **argv) {
  /**
   * Check if the duration parameter is passed.
   */
  if (argc != 2)
    std::cout << "USAGE: throughput_subscriber [DURATION]" << std::endl;
  else {
    /**
     * Initialize the variables.
     * Use the command line arguments from **argv
     */
    double duration = atof(argv[1]);

    /**
     * Initialize ROS environment and a node name, throughput_subscriber.
     */
    ros::init(argc, argv, "throughput_subscriber");

    /**
    * Create the handle for the node, and actually initialize it.
    */
    ros::NodeHandle n;
    
    /**
    * Node will publish on the topic o benchmark_throughput_reporting to the reporter node.
    */
    ros::Publisher pub = n.advertise<benchmark_throughput::ThroughputReportingMessage>("benchmark_throughput_reporting", 10);

    /**
    * Node will receive the actual message on the topic on benchmark_throughput.
    * If it receives a message on this topic, throughputCallback is called.
    * The nodes own publisher is added by binding it to the callback.
    * boost::bind will pass the pub variable to the function, _1 is the number of placeholders,
    * because the first parameter of the function is the message pointer.
    * Basically the same as calling throughputCallback(throughputMessage, stop, counter, pub), but there is no other way
    * in ROS to pass a function with more parameters to subscribe.
    */ 
    int64_t counter = 0;
    int64_t stop = ros::Time(duration).toNSec();
    ros::Subscriber throughput_sub = n.subscribe<benchmark_throughput::ThroughputMessage>("benchmark_throughput", 10, 
      boost::bind(throughputCallback, _1, stop, counter, pub));

    ros::spin();
  }
  return 0;
}