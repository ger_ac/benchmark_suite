#!/usr/bin/env python

import rospy
import sys

from benchmark_throughput.msg import ThroughputMessage

def throughput_publisher(message_size, frequency):
    """
    Setup the throughput_publisher node to publish messages to the benchmark_throughput topic at the given message size and frequency.
    """
    pub = rospy.Publisher('benchmark_throughput', ThroughputMessage, queue_size=10)
    rospy.init_node('throughput_publisher', anonymous=True)
    rate = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        # Create a message_size length array filled with 0 as message data.
        # Each element is 1 byte in the memory.        
        message = ThroughputMessage(data=([0] * message_size))

        try :
            pub.publish(message)
        except rospy.ROSException:
            rospy.signal_shutdown('benchmark_throughput topic closed')

        rospy.loginfo('Message sent')
        rate.sleep()

if __name__ == '__main__':
    # Parse args and pass to throughput_publisher.
    args = sys.argv
    if len(args) != 3:
        print 'USAGE: throughput_publisher.py [MESSAGE_SIZE] [FREQUENCY]'
    else:
        message_size = int(args[1])
        frequency = int(args[2])
        try:
            throughput_publisher(message_size, frequency)
        except rospy.ROSInterruptException:
            pass