#!/usr/bin/env python

import sys
import rospy
import csv
from itertools import groupby
from os import path

from benchmark_throughput.msg import ThroughputReportingMessage

throughput_report = []

def throughput_reporter(message_size, frequency, duration, delta_t, implementation):
    """
    Setup the reporter node to listen to the benchmark_throughput_reporting topic.
    """
    def reporting_callback(message, delta_t):
        """
        Record a message reporting a given throughput, produce report when enough messages recorded and shut down.
        """
        global throughput_report

        if message.message_count == -1:
            evaluate_data(message_size, frequency, duration, delta_t, implementation)
            rospy.signal_shutdown('Reporting finished, reporter shuts down')
        else:
            throughput_report.append(message)        
    
    rospy.init_node('throughput_reporter', anonymous=True)
    rospy.Subscriber('benchmark_throughput_reporting', ThroughputReportingMessage, reporting_callback, delta_t, queue_size=10)

    # Keep the node running until it is shutdown.
    rospy.spin()

def evaluate_data(message_size, frequency, duration, delta_t, implementation):
    """
    Parse the arraylist of messages and create the data which can be print it to csv.
    """
    global throughput_report
    start_time = throughput_report[0].time_received.to_nsec()
    
    # Average throughput value.
    throughput = (float)(throughput_report[-1].message_count * message_size) / (duration)
    export_to_csv_average('benchmark_throughput_average.csv', throughput, throughput_report[-1].message_count, message_size, frequency, duration, implementation)
    
    # Variation in throughput.
    csv_data = { x: 0 for x in range(0, rospy.Time(duration).to_nsec() / rospy.Time(delta_t).to_nsec() + 1) }

    for key, group in groupby(throughput_report, lambda x: (x.time_received.to_nsec() - start_time)/rospy.Time(delta_t).to_nsec()):
        group_list = list(group)
        rel_counter = len(group_list)
        csv_data[key + 1] = rel_counter

    for key, value in csv_data.iteritems():   
        export_to_csv('benchmark_throughput.csv', delta_t * key, value, message_size, frequency, delta_t, implementation)

def export_to_csv_average(file_name, throughput, message_count, message_size, frequency, duration, implementation):
    """
    Export the benchmark data to csv format, appending if the file already exists.
    """
    has_existing_report = path.exists(file_name)

    file_mode = 'a+' if has_existing_report else 'w+'

    with open(file_name, file_mode) as f:
        writer = csv.writer(f)

        if not has_existing_report:
            # Write header row. 
            writer.writerow(['throughput', 'message_count', 'message_size', 'frequency', 'duration', 'implementation'])
        # Write actual content.
        writer.writerow([throughput, message_count, message_size, frequency, duration, implementation])

def export_to_csv(file_name, rel_time, message_count, message_size, frequency, delta_t, implementation):
    """
    Export the benchmark data to csv format, appending if the file already exists.
    """

    has_existing_report = path.exists(file_name)

    file_mode = 'a+' if has_existing_report else 'w+'

    with open(file_name, file_mode) as f:
        writer = csv.writer(f)

        if not has_existing_report:
            # Write header row. 
            writer.writerow(['rel_time', 'message_count', 'message_size', 'frequency', 'delta_t', 'implementation'])
        # Write actual content.
        writer.writerow([rel_time, message_count, message_size, frequency, delta_t, implementation])

if __name__ == '__main__':
    # Parse args and pass to publisher.
    args = sys.argv
    if len(args)  != 6:
        print 'USAGE: throughput_reporter.py [MESSAGE_SIZE] [FREQUENCY] [DURATION] [DELTA_T] [IMPLEMENTATION]'
    else:
        message_size = int(args[1])
        frequency = int(args[2])
        duration = int(args[3])
        delta_t = float(args[4])
        implementation = str(args[5])
        try:
            throughput_reporter(message_size, frequency, duration, delta_t, implementation)
        except rospy.ROSInterruptException:
            pass