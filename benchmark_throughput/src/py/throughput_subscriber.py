#!/usr/bin/env python

import rospy
import sys

from benchmark_throughput.msg import ThroughputMessage, ThroughputReportingMessage

counter = 0
stop = 0

def throughput_subscriber(duration):
    """
    Setup the throughput_subscriber node to listen to the benchmark_throughput topic.
    """
    global stop
    stop = rospy.Time.from_sec(duration).to_nsec()
    # Setup publisher to the reporter.
    reporting_publisher = rospy.Publisher('benchmark_throughput_reporting', ThroughputReportingMessage, queue_size=10)
    
    def throughput_callback(message):
        """
        Measure the throughput and publish to the reporter node.
        """
        global counter
        global stop
        if counter is 0:
            stop = stop + rospy.get_rostime().to_nsec()
        if rospy.get_rostime().to_nsec() > stop:
            # Send a message with counter = -1 to the reporter to indicate that the test has been ended.
            reporting_msg = ThroughputReportingMessage(message_count=-1)
            reporting_msg.time_received = rospy.get_rostime()
            try:
                reporting_publisher.publish(reporting_msg)
            except rospy.ROSException:
                rospy.signal_shutdown('benchmark_throughput_reporting topic closed')
            rospy.signal_shutdown('Test finished')
        else:
            counter = counter + 1
            # Send to the reporting node.
            reporting_msg = ThroughputReportingMessage(message_count=counter)
            reporting_msg.time_received = rospy.get_rostime()
            try:
                reporting_publisher.publish(reporting_msg)
            except rospy.ROSException:
                rospy.signal_shutdown('benchmark_throughput_reporting topic closed')
            rospy.loginfo('Message received %s', str(counter)) 

    rospy.init_node('throughput_subscriber', anonymous=True)
    rospy.Subscriber('benchmark_throughput', ThroughputMessage, throughput_callback, queue_size=10)
    rospy.spin()

if __name__ == '__main__':
    # Parse args and pass to throughput_publisher.
    args = sys.argv
    if len(args) != 2:
        print 'USAGE: throughput_subscriber.py [DURATION]'
    else:
        duration = int(args[1])
        try:
            throughput_subscriber(duration)
        except rospy.ROSInterruptException:
            pass