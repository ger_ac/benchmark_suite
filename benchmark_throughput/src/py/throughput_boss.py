#!/usr/bin/env python

import rospy
import rosgraph
import subprocess
import sys
import time
import os

def throughput_boss(message_size, frequency, duration, delta_t, implementation):
    """
    Run the publisher, subscriber and reporter nodes.
    """

    implementation_suffix = '.py' if implementation == 'python' else ''

    publisher_cmd = 'rosrun benchmark_throughput throughput_publisher%s %s %s' % (implementation_suffix, message_size, frequency)
    subscriber_cmd = 'rosrun benchmark_throughput throughput_subscriber%s %s' % (implementation_suffix, duration)
    reporter_cmd = 'rosrun benchmark_throughput throughput_reporter.py %s %s %s %s %s' % (message_size, frequency, duration, delta_t, implementation)

    start = time.time()
    publisher_process = subprocess.Popen(publisher_cmd, shell=True)

    subscriber_process = subprocess.Popen(subscriber_cmd, shell=True)
    reporter_process = subprocess.Popen(reporter_cmd, shell=True)
    
    reporter_process.wait()

    os.system('rosnode kill -a')

    end = time.time()

    print 'Throughput benchmark completed in %.2fs' % (end - start)

def args_are_valid(args):
    """
    Return whether the given arguments are valid for using in the benchmark.
    """

    if len(args) != 6:
        print 'USAGE: throughput_boss.py [MESSAGE_SIZE] [FREQUENCY] [DURATION] [DELTA_T] [IMPLEMENTATION]'
        return False
    elif not rosgraph.is_master_online():
        print 'ROS Master not online, please run roscore'
        return False
    elif args[5] != 'python' and args[5] != 'c++':
        print 'Unrecognized implementation: ' + str(implementation) + '\nPlease choose c++ or python'
        return False
    else:
        return True

if __name__ == '__main__':
    args = sys.argv

    if args_are_valid(args):
        message_size = args[1]
        frequency = args[2]
        duration = args[3]
        delta_t = args[4]
        implementation = args[5]

        throughput_boss(message_size, frequency, duration, delta_t, implementation)