#!/usr/bin/env python
import subprocess
import time

def main():
    frequency = 1000 
    duration = 30
    delta_t = 1     
    
    # Setting variable values for different tests.
    implementations = ['python', 'c++']
    sizes = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
    
    for implementation in implementations:
        # Vary sizes
        for msg_size in sizes: 
            run_boss_script(msg_size, frequency, duration, delta_t, implementation)
            
def run_boss_script(message_size, frequency, duration, delta_t, implementation):
    """
    Spawn throughput_boss.py to run test cases for variant values.
    """
    boss_cmd = 'rosrun benchmark_throughput throughput_boss.py %s %s %s %s %s' % (message_size, frequency, duration, delta_t, implementation)
    boss_process = subprocess.Popen(boss_cmd, shell=True)
    boss_process.wait()
        
if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()
    print 'Throughput experiments completed in %.2fs' % (end - start)