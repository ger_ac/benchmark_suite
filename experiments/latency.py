#!/usr/bin/env python
import subprocess
import time

def main():
    base_frequency = 1000 
    base_message_size = 100 
    num_of_msgs = 10000     
    
    # Setting variable values for different tests.
    implementations = ['python', 'c++']
    sizes = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
    frequencies = [100, 500, 1000, 5000, 10000, 50000, 100000]
    
    for implementation in implementations:
        # Vary sizes
        for msg_size in sizes: 
            run_boss_script(msg_size, base_frequency, num_of_msgs, implementation)
        # Vary frequency
        for frequency in frequencies:
            run_boss_script(base_message_size, frequency, num_of_msgs, implementation)
            
def run_boss_script(message_size, frequency, num_of_msgs, implementation):
    """
    Spawn boss.py to run test cases for variant values.
    """
    boss_cmd = 'rosrun benchmark_latency latency_boss.py %s %s %s %s' % (message_size, frequency, num_of_msgs, implementation)
    boss_process = subprocess.Popen(boss_cmd, shell=True)
    boss_process.wait()

        
if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()
    print 'Latency experiments completed in %.2fs' % (end - start)