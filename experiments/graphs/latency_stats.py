import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
from scipy import stats
import csv

def main(filename, variant):
    results = get_results(filename, variant)

    paired_results = {}
    for implementation in results.keys():
        implementation_results = results[implementation]
        for variant_value in implementation_results:
            if not variant_value in paired_results:
                paired_results[variant_value] = []
            paired_results[variant_value].append(implementation_results[variant_value])

    # we sort the keys by their numeric value so that the plot pairs are in
    # the right order
    variant_values = paired_results.keys()
    variant_values.sort(key=lambda x: int(x))

    output = []

    for value in variant_values:
        data = paired_results[value]
        python_results = data[0]
        cpp_results = data[1]
        # calculate averages
        python_mean = np.average(python_results)
        cpp_mean = np.average(cpp_results)

        # calculate faster implementation
        faster_implementation = 'python' if python_mean < cpp_mean else 'c++'

        # calculate t-test with two independent samples
        t_stat, p_value = stats.ttest_ind(python_results, cpp_results)
        row = [value, python_mean, cpp_mean, t_stat, p_value, faster_implementation]
        output.append(row)

    with open('latency_stats.csv', 'w+') as f:
        writer = csv.writer(f)
        writer.writerow([variant, 'python_mean', 'c++_mean', 't_statistic', 'p_value', 'faster_implementation'])

        for row in output:
            writer.writerow(row)

def get_results(filename, variant):
    results = {
        'python': {},
        'c++': {}
    }

    with open(filename) as f:
        reader = csv.reader(f)

        for row in reader:
            if row[0] == 'latency': # header row
                continue

            message_size = row[1]

            # use message_size==100 as an indicator of whether it is a size or frequency tests
            # as we only want to include data for that variant
            # obviously this is yucky
            if variant == 'message_size' and message_size == '100':
                continue
            elif variant == 'frequency' and message_size != '100':
                continue

            variant_index = 1 if variant == 'message_size' else 2
            variant_value = row[variant_index]

            implementation = row[3]

            if not variant_value in results[implementation]:
                results[implementation][variant_value] = []

            results[implementation][variant_value].append(int(row[0]))
        
        return results

if __name__ == '__main__':
    args = sys.argv
    if (len(args) != 3) or ((not args[2] == 'message_size') and (not args[2] == 'frequency')):
        print 'USAGE: plot.py [INPUT_FILE] [message_size|frequency]'
    else:
        main(args[1], args[2])