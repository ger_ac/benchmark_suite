import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import csv
from os import path, getcwd

def main(filename, variant):
    results = get_results(filename, variant)

    # so rather than have everything align automatically, we manually
    # position each boxplot - this allows us to position implementations
    # for the same message size close together, with bigger gaps between
    # the message sizes pairs

    # first we rearrange the data into the following structure...
    # {
    #   '2': [python_data, cpp_data],
    #   '4': [python_data, cpp_data],
    #   ...
    # }
    # ...because we need the data for both implementations at the same time
    boxplot_paired_results = {}
    for implementation in results.keys():
        implementation_results = results[implementation]
        for variant_value in implementation_results:
            if not variant_value in boxplot_paired_results:
                boxplot_paired_results[variant_value] = []
            boxplot_paired_results[variant_value].append(implementation_results[variant_value])

    # we sort the keys by their numeric value so that the plot pairs are in
    # the right order
    variant_values = boxplot_paired_results.keys()
    variant_values.sort(key=lambda x: int(x))

    # we get an Axes instance because we want to replot the boxes on the same axis
    figure = plt.figure(figsize=(25, 15))
    axes = plt.axes()

    for variant_value in variant_values:
        paired_results = boxplot_paired_results[variant_value]
        variant_value_index = variant_values.index(variant_value)
        position_adjustment = (variant_value_index * 2) + 1
        # we adjust the position of the pair of boxes based on where the pair is in
        # terms of the whole data set - i.e.
        # first pair x positions:
        #   python  @ 1.0
        #   label   @ 1.5
        #   c++     @ 2.0
        # second pair x positions:
        #   python  @ 2.5
        #   label   @ 3.0
        #   c++     @ 3.5
        # this positioning means that the label is always in the centre between the
        # pair of plots
        positions = [variant_value_index + position_adjustment, variant_value_index + position_adjustment + 1]
        boxplot = axes.boxplot(paired_results, positions=positions, widths=0.75, patch_artist=True)
        # finally style the two pair so that they can be distinguished - we cannot
        # just do this with boxprops as that applies to both boxes
        set_boxplot_colours(boxplot, True, 'black')
        set_boxplot_colours(boxplot, False, 'black')

    # set up the x axis so we have enough space and appropriate labels based on the number of
    # different message sizes
    axes.set_xlabel(get_variant_label(variant))
    axes.set_xticklabels(variant_values)
    x_maximum = len(variant_values) * 3
    axes.set_xticks(np.arange(1.5, x_maximum, 3))
    axes.set_xlim(0, x_maximum)
    # set up y axis
    axes.yaxis.grid(True)
    axes.set_yscale('log')
    axes.set_ylabel('Latency (ns)')

    plt.legend(handles=[
        ptch.Patch(hatch='\\', label='Python', facecolor='#eeeeee'),
        ptch.Patch(hatch='//', label='C++', facecolor='#eeeeee')
    ])

    figure_name = '%s_boxplot_%s.png' % (path.splitext(path.basename(filename))[0], variant)
    figure.savefig(figure_name, bbox_inches='tight')

    plt.show()

def get_results(filename, variant):
    results = {
        'python': {},
        'c++': {}
    }

    with open(filename) as f:
        reader = csv.reader(f)

        for row in reader:
            if row[0] == 'latency': # header row
                continue

            message_size = row[1]

            # use message_size==100 as an indicator of whether it is a size or frequency tests
            # as we only want to include data for that variant
            # obviously this is yucky
            if variant == 'message_size' and message_size == '100':
                continue
            elif variant == 'frequency' and message_size != '100':
                continue

            variant_index = 1 if variant == 'message_size' else 2
            variant_value = row[variant_index]

            implementation = row[3]

            if not variant_value in results[implementation]:
                results[implementation][variant_value] = []

            results[implementation][variant_value].append(int(row[0]))
        
        return results

def set_boxplot_colours(boxplot, is_first_box, colour):
    boxes_index = 0 if is_first_box else 1
    patch = boxplot['boxes'][boxes_index]
    patch.set_color(colour)

    patch.set_facecolor('#eeeeee')
    hatch_pattern = '\\' if is_first_box else '//'
    patch.set_hatch(hatch_pattern)

    caps_index = 0 if is_first_box else 2
    plt.setp(boxplot['caps'][caps_index], color=colour)
    plt.setp(boxplot['caps'][caps_index + 1], color=colour)

    whiskers_index = 0 if is_first_box else 2
    plt.setp(boxplot['whiskers'][whiskers_index], color=colour)
    plt.setp(boxplot['whiskers'][whiskers_index + 1], color=colour)

    medians_index = 0 if is_first_box else 1
    plt.setp(boxplot['medians'][medians_index], color='black')

    # setting outlier colours not working
    # setp(boxplot['fliers'][0], color=colour)
    # setp(boxplot['fliers'][1], color=colour)

def get_variant_label(variant):
    return 'Message Size (bytes)' if variant == 'message_size' else 'Frequency (Hz)'

if __name__ == '__main__':
    args = sys.argv
    if (len(args) != 3) or ((not args[2] == 'message_size') and (not args[2] == 'frequency')):
        print 'USAGE: plot.py [INPUT_FILE] [message_size|frequency]'
    else:
        main(args[1], args[2])