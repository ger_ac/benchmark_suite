import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import csv
from os import path, getcwd

def main(filename):
    results = {}
    message_sizes = set()

    with open(filename) as f:
        reader = csv.reader(f)

        for row in reader:
            if row[0] == 'throughput': # header row
                continue
            
            implementation = row[5]
            if not implementation in results:
                results[implementation] = []

            results[implementation].append(float(row[0]))
            message_sizes.add(int(row[2]))

    message_sizes = to_ordered_list(message_sizes)

    figure = plt.figure(figsize=(25,15))
    axes = plt.axes()
    width = 0.25
    x_positions = np.arange(len(message_sizes))
    
    if 'python' in results:
        python_bars = axes.bar(x_positions, results['python'],
            width=width,color='#eeeeee')
        for bar in python_bars:
            bar.set_hatch('\\')

    if 'c++' in results:
        cpp_bars = axes.bar(x_positions + (width + 0.01), results['c++'],
            width=width, color='#eeeeee')
        for bar in cpp_bars:
            bar.set_hatch('//')

    axes.set_xticks(x_positions + 0.4)
    axes.set_xticklabels(message_sizes)
    axes.set_xlabel('Message Size (bytes)')

    axes.set_ylabel('Throughput (bytes/second)')
    axes.set_yscale('log')

    plt.legend(handles=[
        ptch.Patch(hatch='\\', label='Python', facecolor='#eeeeee'),
        ptch.Patch(hatch='//', label='C++', facecolor='#eeeeee')
    ])

    figure_name = '%s_barchart.png' % path.splitext(path.basename(filename))[0]
    figure.savefig(figure_name, bbox_inches='tight')

    plt.show()

def to_ordered_list(the_set):
    as_list = list(the_set)
    as_list.sort()
    return as_list

if __name__ == '__main__':
    args = sys.argv
    if (len(args) != 2):
        print 'USAGE: plot.py [INPUT_FILE]'
    else:
        main(args[1])