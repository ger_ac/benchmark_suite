import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
from os import path, getcwd

def main(filename, message_size):
    results = {}

    with open(filename) as f:
        reader = csv.reader(f)

        for row in reader:
            if row[0] == 'latency': # header row
                continue

            row_implementation = row[5]

            row_message_size = row[2]

            if row_message_size != message_size:
                continue

            results.setdefault(row_implementation, []).append(int(row[1]))

    figure = plt.figure(figsize=(25,15))
    axes = plt.axes()

    for implementation, data in results.iteritems():
        color = 'b' if implementation == 'python' else 'r'
        axes.plot(data, color=color, label=implementation)

    axes.set_xlabel('Relative time')
    axes.set_xticks(range(0, 31))
    axes.set_ylabel('Delta message count')

    plt.legend(loc='lower right')

    figure_name = '%s_linegraph_%s.png' % (path.splitext(path.basename(filename))[0], message_size)
    figure.savefig(figure_name, bbox_inches='tight')

    plt.show()

if __name__ == '__main__':
    args = sys.argv
    if (len(args) != 3):
        print 'USAGE: plot.py [INPUT_FILE] [MESSAGE_SIZE]'
    else:
        main(args[1], args[2])