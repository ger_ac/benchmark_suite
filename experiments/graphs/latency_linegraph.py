import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
from os import path, getcwd

def main(filename, variant, variant_value, implementation):
    results = []

    with open(filename) as f:
        reader = csv.reader(f)

        for row in reader:
            if row[0] == 'latency': # header row
                continue

            row_implementation = row[3]

            if row_implementation != implementation:
                continue

            variant_index = 1 if variant == 'message_size' else 2
            row_variant_value = row[variant_index]

            if row_variant_value != variant_value:
                continue

            results.append(int(row[0]))

    figure = plt.figure(figsize=(25, 15))
    axes = plt.axes()
    axes.plot(results)
    variant_unit = 'bytes' if variant == 'message_size' else 'Hz'
    axes.set_xlabel('Messages for %s %s' % (variant_value, variant_unit))
    axes.set_xticks([0,10000])
    axes.set_xticklabels([])
    axes.set_ylabel('Latency (ns)')
    axes.set_yscale('log')

    figure_name = '%s_linegraph_%s_%s_%s.png' % (path.splitext(path.basename(filename))[0], variant, variant_value, implementation)
    figure.savefig(figure_name, bbox_inches='tight')

    plt.show()

if __name__ == '__main__':
    args = sys.argv
    if (len(args) != 5) or ((not args[2] == 'message_size') and (not args[2] == 'frequency')):
        print 'USAGE: plot.py [INPUT_FILE] [message_size|frequency] [variant_value] [implementation]'
    else:
        main(args[1], args[2], args[3], args[4])