# To set up python environment

## Install virtualenv 

In any directory
```
pip install virtualenv
```
virtualenv is a tool for managing python projects. We use it so that we can have a version of the libaries we use (numpy, scipy, matplotlib) that is just for this library, rather than for your whole system.

## Setup virtualenv

Change directory into benchmark_suite/graphs
Run the following

```
virtualenv env
source env/bin/activate
```

This setups up a virtual environment in the folder `env` and then sets the terminal to use the configuration for it.

## Install dependencies

Change directory into benchmark_suite/graphs
Run the following

```
pip install -r requirements.txt
```

This installs the required dependencies specified in the requirements.txt (numpy, scipy, matplotlib).